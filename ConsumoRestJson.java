package com.stoconsulting.dcc.neon.as.ws;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.*;
public class ConsumoRestJson {
	
	private String rfc;
	private String numCertificado;
	private URL url;
	private String urlconecccion;
	private JSONObject jsonRequest;
	private JSONObject jsonResponse;
	private Integer codigo; 
	private String cadJson;
	
	
	public static void main(String[] args){
	
				
		try {

			ConsumoRestJson consumoRest = new ConsumoRestJson(rfc, 
					numCert, urlvalidaCert);
			String jsonRequest = consumoRest.crearJson();
			String metodo = "POST";
			HttpURLConnection conexion = consumoRest.crearConexion(jsonRequest, metodo);
           if(!consumoRest.validarRespuest(conexion)) {
        	   LOGGER.error("Hubo error en WS valida certificado");
        	   return false;
           }
           consumoRest.obtenerRespuesta(conexion);
           Integer codigo= consumoRest.getCodigoRespuesta();
            if(codigo == null) {
            	LOGGER.error("Error código de respuesta WS valida certificado ");
            	return false;
            }
            LOGGER.info(consumoRest.getJsonObjectResponse());
            return codigo == 1; 
           
        } catch (Exception e) {
            LOGGER.error("Error en la validacion "+ e);
            return false;
        }
	
	}

	public Integer getCodigoRespuesta() {
		return ((Integer)this.jsonResponse.get("codigo"));
	}


	public String getRfc() {
		return rfc;
	}



	public void setRfc(String rfc) {
		this.rfc = rfc;
	}



	public String getNumCertificado() {
		return numCertificado;
	}



	public void setNumCertificado(String numCertificado) {
		this.numCertificado = numCertificado;
	}



	public URL getUrl() {
		return url;
	}



	public void setUrl(URL url) {
		this.url = url;
	}



	public String getCadJson() {
		return cadJson;
	}



	public void setCadJson(String cadJson) {
		this.cadJson = cadJson;
	}



	public ConsumoRestJson(String rfc, String numCertificado, String url ) {
		this.rfc = rfc;
		this.numCertificado = numCertificado;
		this.urlconecccion = url;
	}
	
	public String  crearJson() {
		
		this.jsonRequest = new JSONObject();
		this.jsonRequest.put("rfc",this.rfc);
		this.jsonRequest.put("num_certificate",this.numCertificado);
		return  this.jsonRequest.toString();	
	}
	
	public HttpURLConnection crearConexion(String jsonRequest,String metodo) throws Exception {
		
		
		  URL url = new URL(this.urlconecccion);
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
         
          conn.setDoOutput(true);            
          conn.setRequestMethod(metodo); //"POST"
          conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
          conn.setRequestProperty("Accept", "application/json");

		 OutputStreamWriter wr= new OutputStreamWriter(conn.getOutputStream());
          wr.write(jsonRequest.toString());
          wr.flush();          
          return conn;
	}
	public boolean validarRespuest(HttpURLConnection conexion)  throws Exception {
		boolean valida=true;
        if (conexion.getResponseCode() != 200) {
        	valida=false;
            throw new RuntimeException("Error consumo WS REST : HTTP Error code : "
                    + conexion.getResponseCode());
            
        }
        
        return valida;
	}
	public void obtenerRespuesta(HttpURLConnection conn) throws Exception {
        InputStreamReader in = new InputStreamReader(conn.getInputStream());
        BufferedReader br = new BufferedReader(in);
        String output;
        while ((output = br.readLine()) != null) {
        	   this.jsonResponse= new JSONObject(output);
        }
        br.close();
        conn.disconnect();
	}




	public String getjsonRequest() {
		return this.jsonRequest.toString();
	}




	public JSONObject getJsonObjectResponse() {
		return this.jsonResponse;
	}
	
	
}

